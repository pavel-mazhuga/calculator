import Bank from "./Bank";

const Banks = ({banks, term, amountOfCredit}) => {
    return (
        <div className="banks-inner">
            <p>Предложения банков</p>
            {banks.length > 0 && term > 0 ?
            <ul>
                {banks.map(bank => (
                    <Bank 
                        key={bank.id}
                        rate={bank.rate} 
                        term={term} 
                        totalAmount={amountOfCredit}
                        bankTerm={bank.year}
                        bankName={bank.name}
                    />
                ))}
            </ul> :
            <div>Подходящих предложений не найдено</div>
            }
        </div>
    )
}

export default Banks;