const Bank = ({rate, term, totalAmount, bankName, bankTerm}) => {
    // ежемесячный платеж
    const x = Math.pow((1 + rate / 12 / 100), term * 12);
    const resultTop = (rate / 12 / 100) * x;
    const resultBottom = x - 1;
    const monthlyPayment = Math.round(totalAmount * resultTop / resultBottom);

    return (
        <li className="bank-item">
            <div className="bank-cell">
                <p className="label">Название</p>
                <span>{bankName}</span>
            </div>
            <div className="bank-cell">
                <p className="label">Процентная ставка</p>
                <span>{rate}</span>
            </div>
            <div className="bank-cell">
                <p className="label">Ежемесячная плата</p>
                <span>{monthlyPayment}</span>
            </div>
            <div className="bank-cell">
                <p className="label">Срок До</p>
                <span>{bankTerm}</span>
            </div>
        </li>
    )
}

export default Bank;