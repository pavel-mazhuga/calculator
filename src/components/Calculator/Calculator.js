
import Form from './Form/Form';
import Banks from './Banks/Banks';
import { useState } from 'react';

const banksJson = [
    {
        'id': 1,
        'name': 'Россельхозбанк',
        'rate': '0.01',
        'year': '30'
    },
    {
        'id': 2,
        'name': 'Совкомбанк',
        'rate': '3',
        'year': '20'
    },
    {
        'id': 3,
        'name': 'Сбербанк',
        'rate': '3',
        'year': '40'
    },
    {
        'id': 4,
        'name': 'Сбербанк',
        'rate': '9',
        'year': '50'
    },
    {
        'id': 5,
        'name': 'Сбербанк',
        'rate': '5.7',
        'year': '10'
    },
    {
        'id': 6,
        'name': 'Сбербанк',
        'rate': '5.9',
        'year': '40'
    },
];

const typesSelect = [
    { value: 'child', label: 'для семей с детьми' },
    { value: 'base', label: 'базовая' },
    { value: 'it', label: 'it' }
]

const dataForm = {
    price: 6291330,
    firstPayment: 1887561,
    term: 30
}

const Calcualtor = () => {
    const [formData, setFormData] = useState(dataForm);

    // проверяем, чтоб первоначальный взнос был не больше стоимости квартиры
    if (+formData.firstPayment > +formData.price) {
        setFormData({...formData, firstPayment: formData.price});
    }

    // фильтруем банки
    const newBanks = banksJson.filter(bank => +formData.term <= +bank.year);

    // Сумма кредита
    const amountOfCredit = formData.price - formData.firstPayment;

    const inputChangeHandler = (e) => {
        setFormData({
            ...formData,
            [e.name]: e.value
        });
    }

    return (
        <div className="calculator-inner">
            <Form inputChangeHandler={inputChangeHandler} typesSelect={typesSelect} dataForm={formData} amountOfCredit={amountOfCredit} />
            <Banks banks={newBanks} term={formData.term} amountOfCredit={amountOfCredit} />
        </div>
    )
}

export default Calcualtor;