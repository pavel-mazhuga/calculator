import Input from "../../UI/Input";
import Select from "../../UI/Select";

const Form = ({typesSelect, inputChangeHandler, dataForm, amountOfCredit}) => {
    // процент первоначального взноса
    const paymentPercent = Math.round((dataForm.firstPayment / dataForm.price) * 100);

    return (
        <div className="calculator">
            <p>Параметры кредитования</p>
            <Select
                id="type"
                label="Вид ипотеки"
                options={typesSelect}
            />
            <Input
                id="cost"
                label="Стоимость квартиры"
                name="price"
                value={dataForm.price}
                type="number"
                onChange={(e) => inputChangeHandler(e.target)}
            />
            <Input
                id="first"
                label="Первый взнос"
                name="firstPayment"
                value={dataForm.firstPayment}
                type="number"
                rightSlot={`${paymentPercent}%`}
                onChange={(e) => inputChangeHandler(e.target)}
            />
            <Input
                id="years"
                label="Срок, лет"
                name="term"
                value={dataForm.term}
                type="number"
                onChange={(e) => inputChangeHandler(e.target)}
            />
            <div className="calculator-footer">
                <div className="calculator-label">Сумма кредита</div>
                <div className="calculator-cost">{amountOfCredit}</div>
            </div>
        </div>
    )
}

export default Form;