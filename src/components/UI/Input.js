import classNames from 'classnames';

const Input = ({ id, label, rightSlot, ...props }) => {
    return (
        <div className={classNames('input-block')}>
            {label && <label className="label" htmlFor={id}>
                {label}
            </label>}
            <input {...props} id={id} />
            {rightSlot && <span className="input-percent">{rightSlot}</span>}
        </div>
    )
}

export default Input;