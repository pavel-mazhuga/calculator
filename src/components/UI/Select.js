import React from 'react'
import Select from 'react-select'

const CustomSelect = ({ id, label, options }) => {
    return (
        <div className="input-block">
            {label && <label className="label" htmlFor={id}>
                {label}
            </label>}
            <Select id={id} options={options} defaultValue={options[0]} />
        </div>
    )
}

export default CustomSelect;